all: export

export: clean whirlstrom.zip

whirlstrom.zip:
	zip -r whirlstrom.zip whirlstrom --exclude @exclude.lst

clean:
	rm -f whirlstrom.zip
	rm -f *~
	rm -f whirlstrom/*.blend?
	rm -f whirlstrom/*~
	rm -rf whirlstrom/src/__pycache__
	rm -rf whirlstrom/src/*~

