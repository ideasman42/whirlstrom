#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the motion of the in-game camera. The script keeps the
# players in view, while constraining the motion to prevent the camera from
# looking beyond the edge of the map.

import bge

from mathutils import Vector

from . import circles

CAM_PREV_POS = Vector()
MARGIN = 4.0
ZOOM_MAX = 4.0

_data = {}


def _clamp_camera_data():
    if _data:
        return _data

    sce = bge.logic.getCurrentScene()
    bl = sce.objects["BL"]
    tr = sce.objects["TR"]
    _data["xmin"], _data["ymin"] = bl.worldPosition.xy
    _data["xmax"], _data["ymax"] = tr.worldPosition.xy
    _data["xmid"] = (_data["xmax"] + _data["xmin"]) / 2.0
    _data["ymid"] = (_data["ymax"] + _data["ymin"]) / 2.0

    _data["zmax_x"] = (_data["xmax"] - _data["xmin"]) / 2.0
    _data["zmax_y"] = (_data["ymax"] - _data["ymin"]) / 2.0

    _data["zmax"] = min(_data["zmax_x"], _data["zmax_y"])

    xtot = bge.render.getWindowWidth()
    ytot = bge.render.getWindowHeight()
    if xtot > ytot:
        _data["xaspect"] = 1.0
        _data["yaspect"] = ytot / xtot
    else:
        _data["xaspect"] = xtot / ytot
        _data["yaspect"] = 1.0

    bl.endObject()
    tr.endObject()
    return _data


def _clamp(v, low, high):
    if v < low:
        return low
    elif v > high:
        return high
    else:
        return v


def _clamp_camera(camNewPos):
    data = _clamp_camera_data()

    if camNewPos.z > data["zmax"]:
        camNewPos.z = data["zmax"]

    # X!
    zmax = data["zmax_x"] * data["xaspect"]
    fac = camNewPos.z / zmax
    if fac > 1.0:
        # camNewPos.z = zmax
        fac = 1.0
    offset = ((1.0 - fac) * zmax)
    xclamp_min = data["xmid"] - offset
    xclamp_max = data["xmid"] + offset
    camNewPos.x = _clamp(camNewPos.x, xclamp_min, xclamp_max)

    # Y!
    #~ zmax = data["zmax_y"] * data["yaspect"]
    fac = camNewPos.z / zmax
    if fac > 1.0:
        # camNewPos.z = zmax
        fac = 1.0
    offset = ((1.0 - fac) * zmax)
    yclamp_min = data["ymid"] - offset
    yclamp_max = data["ymid"] + offset
    camNewPos.y = _clamp(camNewPos.y, yclamp_min, yclamp_max)


def update(cont):
    cameraObject = cont.owner
    bounds = []

    for ob in circles.currentPlayers:
        if ob:
            bounds.append(circles.get_anchor(ob).worldPosition.xy)

    xmin = ymin = 1000000.0
    xmax = ymax = -xmin

    for x, y in bounds:
        ymin = min(ymin, y)
        xmin = min(xmin, x)
        ymax = max(ymax, y)
        xmax = max(xmax, x)

    xmid = (xmax + xmin) / 2.0
    ymid = (ymax + ymin) / 2.0

    width = xmax - xmin
    height = ymax - ymin
    maxdim = max(width, height)

    new_z = maxdim + MARGIN
    if new_z < ZOOM_MAX:
        new_z = ZOOM_MAX

    camNewPos = Vector((xmid, ymid, new_z))

    # clamp, we could turn this off for testing
    _clamp_camera(camNewPos)

    CAM_PREV_POS.xyz = \
    cameraObject.worldPosition = \
            camNewPos.lerp(CAM_PREV_POS, 0.98)

    # test
    # cameraObject.worldPosition = camNewPos
