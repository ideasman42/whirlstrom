#
# Copyright 2012 Campbell Barton <ideasman42@gmail.com>
# Copyright 2012 Alex Fraser <alex@phatcore.com>
# Copyright 2012 Bianca Gibson <bianca.rachel.gibson@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# This file controls the overlay scene, where the number of lives remaining are
# displayed.

import math

import bge
import mathutils

from . import circles


def update_lives(c):
    positive = False
    for s in c.sensors:
        if s.positive:
            positive = True
            break
    if not positive:
        return

    sce = bge.logic.getCurrentScene()
    root = sce.objects["GoodLives"]
    _update_lives(root, circles.currentPlayerConfig[0]["lives"])

    if circles.is_two_player():
        
        root = sce.objects.get("EvilLives")
        if root is None:
            root = sce.addObject("EvilLives", "EvilLives")
        _update_lives(root, circles.currentPlayerConfig[1]["lives"])


def _update_lives(root, lives):
    for lifeIcon in root.children:
        lifeIcon["show"] = lifeIcon["lifeNum"] < lives
